CREATE DATABASE db1;
CREATE USER user1 WITH ENCRYPTED PASSWORD 'mypass';

GRANT CONNECT ON DATABASE db1 to user1;

\c db1

GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO user1;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES to user1;

CREATE TABLE articles (id serial PRIMARY KEY, magazines_id int, article_type_id int, author_id int);
COPY articles FROM '/home/vagrant/provision/articles.csv' WITH (FORMAT csv);

CREATE TABLE magazines (id serial PRIMARY KEY, name varchar(32) );
COPY magazines FROM '/home/vagrant/provision/magazines.csv' WITH (FORMAT csv);

CREATE TABLE article_types (id serial PRIMARY KEY, type varchar(32));
COPY article_types FROM '/home/vagrant/provision/article_types.csv' WITH (FORMAT csv);

CREATE TABLE author (id serial PRIMARY KEY, author varchar(32));
COPY author FROM '/home/vagrant/provision/author.csv' WITH (FORMAT csv);