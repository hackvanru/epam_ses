# Final task for the course Systems Engineering School by EPAM

* [Сourse description](https://training.epam.com/Training/Details/3149?lang=en)
* [Lectures](https://www.youtube.com/playlist?list=PLcV0FNC_1srCduyG-BQRbNQ8jye36BNMJ)
* [Task](Systems_Engineering_School_Final_Task.docx)
* [Defense presentation](Final_task_presentation.pptx)

## Prerequireiments

``` bash
vagrant box add ubuntu/impish64
```

## Run

``` bash
 # First run:
VAGRANT_EXPERIMENTAL="disks" vagrant up

# All next run:
vagrant up
```

## SSH
``` bash
$ vagrant status
Current machine states:

router                    running (virtualbox)
db                        running (virtualbox)
web                       running (virtualbox)

$ vagrant connect router # OR db OR web machine
```