#!/bin/sh

echo "10.10.10.2 web web.local" >> /etc/hosts
echo "10.10.20.2 db db.local" >> /etc/hosts

export DEBIAN_FRONTEND=noninteractive
apt update

apt install -y ntp net-tools tcpdump
timedatectl set-timezone Europe/Moscow

sysctl -w net.ipv4.ip_forward=1
sed '/net.ipv4.ip_forward=1/s/^#//g' -i /etc/sysctl.conf 

# For silient install iptables-persistent (without ncurses prompt)
debconf-set-selections <<EOF
iptables-persistent iptables-persistent/autosave_v4 boolean true
iptables-persistent iptables-persistent/autosave_v6 boolean true
EOF

apt install -y iptables-persistent
cp /home/vagrant/provision/rules.v4 /etc/iptables/rules.v4
iptables-restore < /etc/iptables/rules.v4

# Prometheus & Node exporter
apt -y install prometheus

# Install and configure Certificate Authority (CA)
apt -y install easy-rsa

mkdir /home/vagrant/easy-rsa/
chmod 700 /home/vagrant/easy-rsa
ln -s /usr/share/easy-rsa/* /home/vagrant/easy-rsa
cp /home/vagrant/provision/vars /home/vagrant/easy-rsa/vars
cd /home/vagrant/easy-rsa/

./easyrsa init-pki
./easyrsa --batch --req-cn='router.local CA' build-ca nopass
chown -R vagrant:vagrant /home/vagrant/easy-rsa

cp /home/vagrant/easy-rsa/pki/ca.crt /home/vagrant/pki/ca.crt

# Generate private key web.local
cd /home/vagrant/pki
openssl genrsa -out web.local.key

# Generate CSR web.local
openssl req -new -key web.local.key -out web.local.req -subj \
/C=RU/ST=Saint\ Petersburg/L=Saint\ Petersburg/O=Tricolor/OU=Community/CN=web.local

# Signed certificate web.local
cd /home/vagrant/easy-rsa/
./easyrsa import-req /home/vagrant/pki/web.local.req web.local
./easyrsa --batch sign-req server web.local

cp /home/vagrant/easy-rsa/pki/issued/web.local.crt /home/vagrant/pki/web.local.crt

echo "Router machine provison is done!"