#!/bin/bash

# Include config file
source /home/vagrant/script2_cfg.sh

echo $$ > "$pidfile"

function terminate {
    rm "$pidfile"
    exit $?
}

trap terminate SIGINT
trap terminate SIGTERM

while true
do
    [ ! -d "$backupdir" ] && echo "Error: Directory "$backupdir" does not exists." && exit 1
    
    flag=0
    message=""
    backup_files=`ls "$backupdir" | wc -l`
    # Config variable $threshold_files - see in file script2_cfg.sh
    if [ $backup_files -gt $threshold_files ]
    then
        flag=1
        message="Files count $backup_files in $backupdir greater then threshold $threshold_files."

        #echo "Backup count: $backup_files"
        #echo "Threshold files count: $threshold_files"
    fi

    backup_size=`du -b "$backupdir" | cut -f1`
    # Config variable $threshold_size - see in file script2_cfg.sh
    if [ $backup_size -gt $threshold_size ]
    then
        [ $flag -eq 1 ] && message='${message}\r\n'
        flag=1

        message="${message}Backup size $backup_size bytes in $backupdir greater then threshold $threshold_size bytes"
        #echo "Backup size: $backup_size"
        #echo "Threshold backup size: $threshold_size"
    fi

    if [ $flag -eq 1 ]
    then
        #echo -e $message
        echo -e $message | mail -s 'Alarm! Threshold exceeded.' -a From:Script2\<script2@web\> root@web
    fi

    sleep $check_interval
done