<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>EPAM - Final Task by Vladislav Charikov</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">
  </head>
  <body>
    
<header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">Epam - Final task by Vladislav Charikov</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
      <!--
        <ul class="navbar-nav me-auto mb-2 mb-md-0">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="#">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled">Disabled</a>
          </li>
        </ul>
        <form class="d-flex">
          <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
      -->
      </div>
    </div>
  </nav>
</header>

<main>
  <div class="container marketing">

{% for item in articles %}

  {% if loop.index is divisibleby 2 %}
    <hr class="featurette-divider">

    <div class="row featurette">
      <div class="col-md-7 order-md-2">
        <h2 class="featurette-heading">Category: {{item.type}} <span class="text-muted">Author: {{item.author}}</span></h2>
        <p class="lead">{{item.name}}</p>
        <p>{{ lipsum(1) }}</p>
      </div>
      <div class="col-md-5 order-md-1">
         {% if item.article_type == 1 %}<img src="img/news.png"></img>{% endif %}
         {% if item.article_type == 2 %}<img src="img/tech.png"></img>{% endif %}
         {% if item.article_type == 3 %}<img src="img/ent.png"></img>{% endif %}
      </div>
    </div>
  {% else %}
    <hr class="featurette-divider">

    <div class="row featurette">
      <div class="col-md-7">
        <h2 class="featurette-heading">Category: {{item.type}} <span class="text-muted">Author: {{item.author}}</span></h2>
        <p class="lead">{{item.name}}</p>
        <p>{{ lipsum(1) }}</p>
      </div>
      <div class="col-md-5">
         {% if item.article_type == 1 %}<img src="img/news.png"></img>{% endif %}
         {% if item.article_type == 2 %}<img src="img/tech.png"></img>{% endif %}
         {% if item.article_type == 3 %}<img src="img/ent.png"></img>{% endif %}
      </div>
    </div>
   {% endif %}

{% endfor %}

    <hr class="featurette-divider">

    <!-- /END THE FEATURETTES -->

  </div><!-- /.container -->


  <!-- FOOTER -->
  <footer class="container">
    <p class="float-end"><a href="#">Back to top</a></p>
    <p>&copy; 2022 Vladislav Charikov &middot; Page generation time {{ date_time }} &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
  </footer>
</main>
      
  </body>
</html>