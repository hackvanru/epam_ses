#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime

import psycopg2
import psycopg2.extras
from jinja2 import Template

sql = '''SELECT A.id, AU.author, AT.id AS article_type , AT.type, M.name 
          FROM articles A
          INNER JOIN magazines M ON A.magazines_id = M.id
          INNER JOIN author AU ON A.author_id = AU.id
          INNER JOIN article_types AT ON A.article_type_id = AT.id'''

conn = psycopg2.connect(dbname='db1', user='user1', 
                        password='mypass', host='db')
cursor = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
cursor.execute(sql)
articles = cursor.fetchall()

#print(articles)

html = open('index.tpl').read()
template = Template(html)

now = datetime.now()
date_time = now.strftime("%d/%m/%Y, %H:%M:%S")

output = template.render(articles=articles, date_time=date_time)

with open('/var/www/html/index.html', 'w') as f:
    f.write(output)