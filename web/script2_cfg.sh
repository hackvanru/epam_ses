#!/bin/bash

pidfile=/var/run/script2.pid

backupdir=/local/backups/

check_interval=5m

# Number of files in /local/backups directory is more than X
threshold_files=3

# Total size of /local/backups directory is more than Y bytes
threshold_size=600