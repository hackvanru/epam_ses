#!/bin/bash

destdir=/local/files/
backupdir=/local/backups/

sqlquery="SELECT A.id, AU.author, AT.type, M.name 
          FROM articles A
          INNER JOIN magazines M ON A.magazines_id = M.id
          INNER JOIN author AU ON A.author_id = AU.id
          INNER JOIN article_types AT ON A.article_type_id = AT.id
          ORDER BY RANDOM()
          LIMIT 1"

[ ! -d "$destdir" ] && echo "Error: Directory "$destdir" does not exists." && exit 1
[ ! -d "$backupdir" ] && echo "Error: Directory "$backupdir" does not exists." && exit 1

csv=`echo "$sqlquery" | psql -h db -U user1 -w db1 --csv -t`

[ -z "$csv" ] && echo "Error: SQL responce is empty." && exit 1

article_id=`echo $csv | cut -d',' -f1`

printf -v path "%s%s.csv" $destdir $article_id
echo "$csv" > "$path"

file_count=`ls "$destdir" | wc -l`

if [ "$file_count" -gt 3 ]
then
	backup_count=`ls "$backupdir" | wc -l`
	if [ "$backup_count" -ge 1 ]
	then
        path="${backupdir}articles.tar.gz.${backup_count}"
        tar -zcf $path $destdir
     else
     	path="${backupdir}articles.tar.gz"
        tar -zcf $path $destdir
     fi

     rm -f "$destdir"*
fi


