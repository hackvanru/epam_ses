#!/bin/sh

echo "10.10.20.1 router router.local" >> /etc/hosts
echo "10.10.10.2 web web.local" >> /etc/hosts

#: '
# Make RAID
mdadm --zero-superblock --force /dev/sdc /dev/sdd
wipefs --all --force /dev/sdc /dev/sdd
echo "y" | sudo mdadm --create --verbose /dev/md0 -l 1 -n 2 /dev/sdc /dev/sdd

# Make mdadm.conf
echo "DEVICE partitions" >> /etc/mdadm/mdadm.conf
mdadm --detail --scan --verbose | awk '/ARRAY/ {print}' >> /etc/mdadm/mdadm.conf
update-initramfs -u

# Make and mount filesystem
mkfs.ext4 /dev/md0
mkdir -p /local/backups
mount /dev/md0 /local/backups
echo "/dev/md0        /local/backups    ext4    defaults    1 2" >> /etc/fstab

# Make LVM
pvcreate /dev/sde /dev/sdf
vgcreate vol_grp1 /dev/sde  /dev/sdf
lvcreate -L 1.99G -n logical_vol1 vol_grp1

# Make and mount filesystem
mkfs.ext4 /dev/vol_grp1/logical_vol1
mkdir -p /local/files
mount /dev/vol_grp1/logical_vol1 /local/files
echo "/dev/vol_grp1/logical_vol1       /local/files    ext4    defaults    1 2" >> /etc/fstab
#'

#
# TEMP: Remove after uncomment code for make disks
#
#mkdir -p /local/files
#mkdir -p /local/backups
#chown -R vagrant:vagrant /local/files /local/backups

export DEBIAN_FRONTEND=noninteractive
apt update

apt install -y ntp net-tools
timedatectl set-timezone Europe/Moscow


# For silient install iptables-persistent (without ncurses prompt)
debconf-set-selections <<EOF
iptables-persistent iptables-persistent/autosave_v4 boolean true
iptables-persistent iptables-persistent/autosave_v6 boolean true
EOF

apt install -y iptables-persistent
cp /home/vagrant/provision/rules.v4 /etc/iptables/rules.v4
iptables-restore < /etc/iptables/rules.v4

apt -y install postgresql

# Need for psql access to folder /home/vagrant
chmod 755 /home/vagrant
sudo -u postgres psql -f /home/vagrant/provision/init.sql

# Enable listen interfaces Postgress on 10.10.20.2,192.168.56.3
sed -i "/listen/s/\#listen_addresses\ =\ 'localhost'/listen_addresses = '10.10.20.2,192.168.56.3'/g" -i /etc/postgresql/13/main/postgresql.conf

# Allow connection from host web to user - user1 in database db1
echo "host db1 user1 web md5" >> /etc/postgresql/13/main/pg_hba.conf
echo "host db1 user1 192.168.56.1/32 md5" >> /etc/postgresql/13/main/pg_hba.conf

systemctl restart postgresql

apt install -y nfs-kernel-server

echo "/local/files     web(rw,sync,no_root_squash,no_subtree_check)" >> /etc/exports
echo "/local/backups   web(rw,sync,no_root_squash,no_subtree_check)" >> /etc/exports

systemctl restart nfs-kernel-server

# Prometheus & Node exporter
apt -y install prometheus

# Install root sertificate CA
cp /home/vagrant/pki/ca.crt /usr/local/share/ca-certificates/ca.crt
update-ca-certificates

echo "DB machine provision is done!"