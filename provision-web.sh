#!/bin/sh

echo "10.10.10.1 router router.local" >> /etc/hosts
echo "10.10.20.2 db db.local" >> /etc/hosts

export DEBIAN_FRONTEND=noninteractive
apt update

apt install -y ntp net-tools
timedatectl set-timezone Europe/Moscow

# For silient install iptables-persistent (without ncurses prompt)
debconf-set-selections <<EOF
iptables-persistent iptables-persistent/autosave_v4 boolean true
iptables-persistent iptables-persistent/autosave_v6 boolean true
EOF

apt install -y iptables-persistent
cp /home/vagrant/provision/rules.v4 /etc/iptables/rules.v4
iptables-restore < /etc/iptables/rules.v4

apt install -y nfs-common

mkdir -p /local/files
mkdir -p /local/backups

mount db:/local/files    /local/files
mount db:/local/backups  /local/backups

echo "db:/local/files    /local/files    nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0" >> /etc/fstab
echo "db:/local/backups  /local/backups  nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0" >> /etc/fstab

apt install -y nginx

chmod 757 /var/www/html/
rm /var/www/html/index.nginx-debian.html

apt install -y postgresql-client-common postgresql-client-13

cp /home/vagrant/provision/.pgpass /home/vagrant/.pgpass
chmod 0600 /home/vagrant/.pgpass
chown vagrant:vagrant /home/vagrant/.pgpass

cp /home/vagrant/provision/script1.sh /home/vagrant/script1.sh
chmod +x /home/vagrant/script1.sh
chown vagrant:vagrant /home/vagrant/script1.sh


# For silient install mailutils (without ncurses prompt)
debconf-set-selections <<EOF 
postfix postfix/mailname string web.local
postfix postfix/main_mailer_type string 'Local only'
EOF

apt install -y mailutils

cp /home/vagrant/provision/script2.sh /home/vagrant/script2.sh
chmod +x /home/vagrant/script2.sh
chown vagrant:vagrant /home/vagrant/script2.sh

cp /home/vagrant/provision/script2_cfg.sh /home/vagrant/script2_cfg.sh
chmod +x /home/vagrant/script2_cfg.sh
chown vagrant:vagrant /home/vagrant/script2_cfg.sh

cp /home/vagrant/provision/script2.service /lib/systemd/system/script2.service

systemctl daemon-reload
systemctl enable script2.service 
systemctl start script2.service


cp /home/vagrant/provision/script3/script3.py /home/vagrant/script3.py
cp /home/vagrant/provision/script3/index.tpl /home/vagrant/index.tpl
chown vagrant:vagrant /home/vagrant/script3.py
chown vagrant:vagrant /home/vagrant/index.tpl

cp -R /home/vagrant/provision/script3/css/ /var/www/html/
cp -R /home/vagrant/provision/script3/img/ /var/www/html/

apt -y install python3-venv

python3 -m venv .env
# Use . instead source command
. /home/vagrant/.env/bin/activate
pip install -r /home/vagrant/provision/script3/requirements.txt
deactivate
chown -R vagrant:vagrant /home/vagrant/.env/

sudo -u vagrant crontab /home/vagrant/provision/script3/script3.crontab


# Prometheus & Node exporter
apt -y install prometheus

# Grafana
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
add-apt-repository -y "deb https://packages.grafana.com/oss/deb stable main"
apt update

apt -y install grafana

# Provision grafana datasource and dashboards
cp /home/vagrant/provision/grafana/datasource.yml /etc/grafana/provisioning/datasources/datasource.yml
cp /home/vagrant/provision/grafana/dashboard.yml /etc/grafana/provisioning/dashboards/dashboard.yml
cp /home/vagrant/provision/grafana/web.json /etc/grafana/provisioning/dashboards/web.json
cp /home/vagrant/provision/grafana/db.json /etc/grafana/provisioning/dashboards/db.json
cp /home/vagrant/provision/grafana/router.json /etc/grafana/provisioning/dashboards/router.json

systemctl daemon-reload
systemctl enable grafana-server
systemctl start grafana-server


# Elasticsearch
curl -fsSL https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
apt update
apt -y install elasticsearch

systemctl enable elasticsearch
systemctl start elasticsearch


# Kibana
apt -y install kibana

systemctl enable kibana
systemctl start kibana

echo "admin:`openssl passwd -apr1 kibana`" | tee -a /etc/nginx/htpasswd.users

cp /home/vagrant/provision/kibana.nginx /etc/nginx/sites-available/kibana
ln -s /etc/nginx/sites-available/kibana /etc/nginx/sites-enabled/kibana
systemctl reload nginx

# Logstash
apt -y install logstash

cp /home/vagrant/provision/logstash/02-beats-input.conf /etc/logstash/conf.d/02-beats-input.conf
cp /home/vagrant/provision/logstash/30-elasticsearch-output.conf /etc/logstash/conf.d/30-elasticsearch-output.conf

systemctl enable logstash
systemctl start logstash


apt -y install filebeat

# Comment elasticsearch and uncomment logstash
sed -Ei '/^output.elasticsearch:$/,/^.*hosts: \["localhost:9200"\]$/{s/(^.*$)/#\1/}' /etc/filebeat/filebeat.yml
sed '/output.logstash/s/^#//g' -i /etc/filebeat/filebeat.yml
sed -Ei '/(^.*#hosts: \["localhost:5044"\]$)/{s/(^.*#)(.*$)/  \2/g}' /etc/filebeat/filebeat.yml

filebeat modules enable system
filebeat setup --pipelines --modules system
filebeat setup --index-management -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]'
filebeat setup -E output.logstash.enabled=false -E output.elasticsearch.hosts=['localhost:9200'] -E setup.kibana.host=localhost:5601

systemctl enable filebeat
systemctl start filebeat

### Part of provision code of web.local
# Install root sertificate CA
cp /home/vagrant/pki/ca.crt /usr/local/share/ca-certificates/
update-ca-certificates

# Certificate for nginx
cp /home/vagrant/pki/web.local.crt /etc/nginx/web.local.crt
cp /home/vagrant/pki/web.local.key /etc/nginx/web.local.key

cp /home/vagrant/provision/default.nginx /etc/nginx/sites-available/default
systemctl reload nginx


echo "Web machine provision is done!"