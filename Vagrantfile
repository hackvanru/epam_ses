# MGMT: NAT
# External: bridget to network in physical station (e.g 192.168.1.0/24)
# Intranet-1: 10.10.10.0/24
# Intranet-2: 10.10.20.0/24

#
# Warning!!! run 'VAGRANT_EXPERIMENTAL="disks" vagrant up' for provision additional disks
#

Vagrant.configure("2") do |config|
  config.vm.define "router" do |router|
    router.vm.box = "ubuntu/impish64"
    #router.vm.box = "ubuntu-hackvan/impish64"
    router.vm.hostname = "router.local" 
    
    router.vm.network :private_network, ip: "10.10.10.1",
      virtualbox__intnet: "intranet-1"
    router.vm.network :private_network, ip: "10.10.20.1",
      virtualbox__intnet: "intranet-2"

    router.vm.network "forwarded_port", guest: 22, host: 2221, id: "ssh"

    router.vm.provider "virtualbox" do |v|
      v.name = "router"
      v.memory = "1024"
      v.cpus = 1
    end

    router.vm.synced_folder ".", "/vagrant", disabled: true
    router.vm.synced_folder "router", "/home/vagrant/provision"
    router.vm.synced_folder "pki", "/home/vagrant/pki"
    router.vm.provision "shell", path: "provision-router.sh"
  end


  config.vm.define "db" do |db|
    db.vm.box = "ubuntu/impish64"
    #db.vm.box = "ubuntu-hackvan/impish64"
    db.vm.hostname = "db.local" 

    db.vm.network :private_network, ip: "10.10.20.2", hostname: true,
      virtualbox__intnet: "intranet-2"

    db.vm.network "forwarded_port", guest: 22, host: 2222, id: "ssh"

    (1..4).each do |i|
        db.vm.disk :disk, size: "1GB", name: "disk-#{i}"
    end

    db.vm.provider "virtualbox" do |v|
      v.name = "db"
      v.memory = "1024"
      v.cpus = 1
    end

    db.vm.synced_folder ".", "/vagrant", disabled: true
    db.vm.synced_folder "db", "/home/vagrant/provision"
    db.vm.synced_folder "pki", "/home/vagrant/pki"
    
    db.vm.provision "shell",
      run: "always",
      inline: "ip route add 10.10.10.0/24 via 10.10.20.1"
    db.vm.provision "shell", path: "provision-db.sh"
  end


  config.vm.define "web", primary: true do |web|
    web.vm.box = "ubuntu/impish64"
    #web.vm.box = "ubuntu-hackvan/impish64"
    web.vm.hostname = "web.local" 

    web.vm.network :private_network, ip: "10.10.10.2", hostname: true,
      virtualbox__intnet: "intranet-1"
    # External network
    web.vm.network "public_network", ip: "192.168.1.100"

    web.vm.network "forwarded_port", guest: 22, host: 2223, id: "ssh"
    web.vm.network "forwarded_port", guest: 3000, host: 3000, id: "Grafana"
    web.vm.network "forwarded_port", guest: 4000, host: 4000, id: "Kibana"

    web.vm.provider "virtualbox" do |v|
      v.name = "web"
      v.memory = "5120"
      v.cpus = 4
  end

    web.vm.synced_folder ".", "/vagrant", disabled: true
    web.vm.synced_folder "web", "/home/vagrant/provision"
    web.vm.synced_folder "pki", "/home/vagrant/pki"

    web.vm.provision "shell",
      run: "always",
      inline: "ip route add 10.10.20.0/24 via 10.10.10.1"
    web.vm.provision "shell", path: "provision-web.sh"
  end

end